function Game () {
    this.state = "init";
    this.result = -1;
    this.symbol_buttons = [];
    this.selected_option = 1;
    this.board = new Board();
    this.reload_button = null;
    this.select_button = null;
    this.spin_button = null;

    this.initialize_options = function (){
        this.select_button = new RectButton(1060, 45, 200, 125, fruits[this.selected_option], fruits[this.selected_option], null, function() {
            game.state = "selection";
        });
        this.reload_button = new RectButton( 450, 400, 206, 80, asset_manager.getAsset("again"), asset_manager.getAsset("again_active"), null, function() {
            game.state = "ready";
        });

        var spinActiveImage = asset_manager.getAsset("btn_spin_active");
        var spinBlockImage = asset_manager.getAsset("btn_spin_block");
        this.spin_button = new ArcButton(1167, 358, 70, spinActiveImage, spinBlockImage, function() {
            game.board.randomize_array();
            setTimeout(function () {
                game.check_result();
            }, 500);
        });

        for (var i = 0; i < 6 ; i++ ){
            var position_x = 130 + 300 * (i - 3 * Math.floor(i/3));
            var position_y = 250 + 180 * Math.floor(i/3);
            this.symbol_buttons[i] =  new RectButton( position_x, position_y, 235, 155, fruits[i], fruits[i], i, function(nr) {
                console.log("change selection on number: " + nr);
                game.selected_option = nr;
                game.state = "ready";
                game.select_button.img = fruits[nr];
                game.select_button.imgBlock = fruits[nr];
            });
        }
    };

    this.check_result = function(){
       this.result = this.board.check_result();
        if ( this.result < 0  ){
            this.state = "ready";
            console.log("LOOSE");
        } else {
            this.state = "win";
            console.log("WINN");
        }
        this.board.state = "ready";
    };

    this.display_result = function () {
        if ( this.state === "win" ){
            var position_x = this.board.start_x - 20;
            var position_y = this.board.start_y  + 80 + 150 * this.result;
            var line = asset_manager.getAsset("Bet_Line");
            ctx.drawImage(line, position_x, position_y, 910, 10 );
            ctx.fillStyle = 'rgba(0, 0, 0,0.6)';
            ctx.fillRect(50, 50, 1000, 600);
            ctx.font = "bold 40px Comic Sans MS";
            ctx.fillStyle = "#d62e19";
            ctx.font = "bold 150px Comic Sans MS";
            ctx.strokeStyle = '#FFF';
            ctx.lineWidth = 12;
            ctx.strokeText("You Win!!!",200,350);
            ctx.fillText("You Win!!!",200,350);
            this.reload_button.update();
            this.reload_button.draw();
        }
    };

    this.display_option = function () {
        if ( this.state === "selection" ){
            ctx.fillStyle = 'rgba(0, 0, 0,0.6)';
            ctx.fillRect(50, 50, 1000, 600);
            ctx.font = "bold 40px Comic Sans MS";
            ctx.fillStyle = "#FFF";
            ctx.fillText("Choose your symbol ...", 180, 150);
            for (var i = 0; i<6 ; i++ ) {
                this.symbol_buttons[i].update();
                this.symbol_buttons[i].draw();
            }
        }
    };

    var counter = 0;
    this.display_loading_screen = function () {
        if( this.state === "init" ){
            counter++;
            ctx.fillStyle = '#000';
            ctx.fillRect(0, 0, canvas.width, canvas.height);
            ctx.font = "bold 40px Comic Sans MS";
            ctx.fillStyle = "#FFF";
            ctx.fillStyle = "#FFF";
            var text = "Loading";
            text += " [ " + asset_manager.successCounter + " / " + asset_manager.queue.length + "]";
            for( var i = 0; i < Math.floor(counter / 50); i ++ ) { text = text + "."}
            ctx.fillText(text, 180, 150);
        }
    }
}
