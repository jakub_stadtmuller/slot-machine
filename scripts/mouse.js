var mouse = {
    position: {
        x: 0,
        y: 0
    },
    mouse_pressed: false
};

canvas.addEventListener('mousemove', function(event) {
    mouse.position.x = event.offsetX || event.layerX;
    mouse.position.y = event.offsetY || event.layerY;
});

canvas.addEventListener('mousedown', function(event) {
    mouse.mouse_pressed = true;
});
canvas.addEventListener('mouseup', function(event) {
    mouse.mouse_pressed = false;
});