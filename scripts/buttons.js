function RectButton(x, y, w, h, img, imgBlock, i, clickedCallback) {
    this.x = x;
    this.y = y;
    this.i = i;
    this.width = w;
    this.height = h;
    this.img = img;
    this.imgBlock = imgBlock;
    this.state = 'default';
    this.is_clicking = false;


    this.update = function() {
        //hover
        if (mouse.position.x >= this.x && mouse.position.x <= this.x + this.width &&
            mouse.position.y >= this.y && mouse.position.y <= this.y + this.height) {
            this.state = 'hover';

            // click
            if (mouse.mouse_pressed) {
                this.state = 'pressed';
                if (typeof clickedCallback === 'function' && !this.is_clicking) {
                    clickedCallback(this.i);

                    this.is_clicking = true;
                }
            } else {
                this.is_clicking = false;
            }
        } else {
            this.state = 'default';
        }
    };

    this.draw = function() {
        var sprite;
        var start_x = this.x;
        var start_y = this.y;
        var end_x = this.width;
        var end_y = this.height;
        switch(this.state) {
            case "hover":
                sprite = this.img;
                start_x = start_x - 5;
                start_y = start_y - 5;
                end_x = end_x + 10;
                end_y = end_y + 10;
                break;
            case "pressed":
                sprite = this.imgBlock;
                break;
            default:
                sprite = this.img;
        }
        ctx.drawImage(sprite,start_x, start_y,end_x, end_y);
    };
}


function ArcButton(x, y, r, img, imgBlock, clickedCallback) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.img = img;
    this.imgBlock = imgBlock;
    this.state = 'default';
    this.is_clicking = false;


    this.update = function() {
        if ( game.state === "ready" && game.board.state !== "animate") {
            var d = Math.sqrt(Math.pow(mouse.position.x - this.x, 2) + Math.pow(mouse.position.y - this.y, 2));
            if (d <= this.r) {
                this.state = 'hover';

                // click
                if (mouse.mouse_pressed) {
                    this.state = 'pressed';
                    if (typeof clickedCallback === 'function' && !this.is_clicking) {
                        clickedCallback();
                        this.is_clicking = true;
                    }
                } else {
                    this.is_clicking = false;
                }
            } else {
                this.state = 'default';
            }
        } else {
            this.state = "disable";
        }
    };

    this.draw = function() {
        var sprite;
        var start_x = this.x - this.r;
        var start_y = this.y - this.r;
        var end_x = 2*this.r;
        var end_y = 2*this.r;
        switch(this.state) {
            case "hover":
                sprite = this.img;
                start_x = start_x - 5;
                start_y = start_y - 5;
                end_x = end_x + 10;
                end_y = end_y + 10;
                break;
            case "disable":
                sprite = this.imgBlock;
                break;
            case "pressed":
                sprite = this.imgBlock;
                break;
            default:
                sprite = this.img;
        }
        ctx.drawImage(sprite,start_x, start_y,end_x, end_y);
    };
}
