function AssetManager (){
    this.queue = [];
    this.successCounter = 0;
    this.errorCounter = 0;
    this.library = {};
    this.state = "init";

    this.parse_json = function(request){
        var json = JSON.parse(request.responseText);
        for (var i = 0; i< json.assets.length; i++ ) {
            this.addToQueue(json.assets[i].name, json.assets[i].path);
        }
        this.state = "file_loaded";
    };

    this.load_data_form_file = function loadFile(callbackFunction) {
        var xhttp = new XMLHttpRequest();
        var that = this;
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                that.parse_json(this);
                callbackFunction();
            }
        };
        xhttp.open("GET", "assets.json", true);
        xhttp.send();
    };

    this.addToQueue = function(name,path) {
        this.queue.push({name: name, path: path});
    };

    this.downloadAll = function(downloadCallback) {
        if (this.queue.length === 0) {
            downloadCallback();
        }
        for (var i = 0; i < this.queue.length; i++) {
            var img = new Image();
            var that = this;
            img.addEventListener("load", function() {
                that.successCounter += 1;
                if (that.isDone()) {
                    downloadCallback();
                }
            }, false);
            img.addEventListener("error", function() {
                that.errorCounter += 1;
                if (that.isDone()) {
                    downloadCallback();
                }
            }, false);
            img.src = this.queue[i]['path'];
            this.library[this.queue[i]['name']] = img;
        }
    };

    this.isDone = function() {
        return (this.queue.length === this.successCounter + this.errorCounter);
    };

    this.getAsset = function(name) {
        return this.library[name];
    }
}