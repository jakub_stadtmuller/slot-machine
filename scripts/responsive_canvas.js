var c_width = 1280;
var c_height = 720;

var fixDimensions = function(){
    var w_width = window.innerWidth;
    var w_height = window.innerHeight;

    var y_ratio = (w_height/c_height);
    var x_ratio = (w_width/c_width);

    if(x_ratio >= y_ratio) {
        viewport.style.transform = "scale("+ y_ratio +")";
        viewport.style.left = ((w_width - c_width * y_ratio)/2) + "px" ;
        viewport.style.top = "0";
    }else{
        viewport.style.transform = "scale("+ x_ratio +")";
        viewport.style.left = "0";
        viewport.style.top = ((w_height - c_height * x_ratio)/2) + "px" ;
    }
};

fixDimensions();
window.addEventListener('resize',fixDimensions);