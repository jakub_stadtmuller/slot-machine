function Element (value, position_x, position_y, destination_x, destination_y, index ){
    this.value = value;
    this.index = index;
    this.velocity = 0;
    this.position = {
        x: position_x,
        y: position_y
    };
    this.destination = {
        x: destination_x,
        y: destination_y
    };

    this.update = function (){
        var position_next_step =  this.position.y + this.velocity + 1 + this.index;

        if (position_next_step >= this.destination.y ) {
            this.position.y = this.destination.y;
        }else{
            this.position.y += this.velocity
            game.board.state = "animate";
        }
        this.velocity += 3 + this.index * 0.5;
    };

    this.draw = function () {
        ctx.drawImage(fruits[this.value], this.position.x, this.position.y, 235, 155);
    }
}
