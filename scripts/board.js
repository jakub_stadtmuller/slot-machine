function Board () {
    this.start_x = 130;
    this.start_y = 80;
    this.mani_array = [];
    this.state = 'init';

    this.randomize_array = function() {
        for (var i = 0; i < 4; i++){
            this.mani_array[i] = [];
            for (var j = 0; j < 3; j++) {
                var destination_x = this.start_x + 320 * j;
                var destination_y = this.start_y + 150 * i;
                var start_x = destination_x;
                var start_y = destination_y - 700;
                this.mani_array[i][j] = new Element(
                    Math.round(Math.random()*5),
                    start_x,
                    start_y,
                    destination_x,
                    destination_y,
                    j + j*i
                );
            }
        }

        //// cheater mode - uncomment ot test a win option
        // this.mani_array[3][0].value = 3;
        // this.mani_array[3][1].value = 3;
        // this.mani_array[3][2].value = 3;
        ////

        this.state = 'ready';
    };

    this.check_result = function (){
        for (var i = 0; i < 4; i++) {
            var flag = true;
            for (var j = 0; j < 3; j++) {
                if (this.mani_array[i][j].value !== game.selected_option) {
                    flag = false;
                    break;
                }
            }
            if(flag){ return i;}
        }
        return -1;
    };

    this.draw = function(){
        if (this.state !== 'init') {
            for (var i = 0; i < 4; i++) {
                for (var j = 0; j < 3; j++) {
                    this.mani_array[i][j].update();
                    this.mani_array[i][j].draw();
                }
            }
        }
    };
}