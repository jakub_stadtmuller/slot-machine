var game = new Game();
var fruits = [];

asset_manager.load_data_form_file(function(){
    asset_manager.downloadAll(function(){
        // only reason to this timeout is to see loading screen a little bit longer
        setTimeout(function () {
            for (var i = 1; i <= 6; i++){
                fruits.push(asset_manager.getAsset("SYM" + i));
            }
            game.initialize_options();
            game.state = "selection";
        }, 2000);
    });
});


function animate() {
    requestAnimationFrame(animate);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if (game.state === "init"){
        game.display_loading_screen();
    }else{
        game.select_button.update();
        game.select_button.draw();
        game.spin_button.update();
        game.spin_button.draw();
        game.board.draw();
        game.display_option();
        game.display_result();
    }
}

requestAnimationFrame(animate);